<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\Auth\ResourceRolesController;
use App\Http\Controllers\Contoh\ContohModulController;
use App\Http\Controllers\Auth\ResourcePermissionsController;
use App\Http\Controllers\Auth\ResourceRoleHasPermissionsController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::post('login', [UserController::class,'login']);
Route::post('register', [UserController::class,'register']);


Route::group(
    ['middleware' => 'auth:api','cors'],
    function(){
    Route::post('details', [UserController::class,'details']);

    Route::get('contohsatu', [ContohModulController::class,'index']);
    Route::post('contohsatu', [ContohModulController::class,'store']);
    Route::get('contohsatu/{id}', [ContohModulController::class,'show']);
    Route::put('contohsatu/{id}', [ContohModulController::class,'update']);
    Route::delete('contohsatu/{id}', [ContohModulController::class,'destroy']);
});


Route::group(
    [
        'middleware' => 'auth:api','cors',
        'namespace'  => 'App\Http\Controllers',
        'prefix'     => 'privileges'
    ],
    function ($router) {
        Route::get('roles/search', [ResourceRolesController::class,'search']);
        Route::get('roles', [ResourceRolesController::class,'index']);
        Route::post('roles', [ResourceRolesController::class,'store']);
        Route::get('role/{id}', [ResourceRolesController::class,'show']);
        Route::put('role/{id}', [ResourceRolesController::class,'update']);
        Route::delete('role/{id}', [ResourceRolesController::class,'destroy']);

        Route::get('permissions/search', [ResourcePermissionsController::class,'search']);
        Route::get('permissions', [ResourcePermissionsController::class,'index']);
        Route::post('permissions', [ResourcePermissionsController::class,'store']);
        Route::get('permission/{id}', [ResourcePermissionsController::class,'show']);
        Route::put('permission/{id}', [ResourcePermissionsController::class,'update']);
        Route::delete('permission/{id}', [ResourcePermissionsController::class,'destroy']);

        Route::get('user-and-role-has-permissions/{id}', [ResourceRoleHasPermissionsController::class,'userrolehasperm']);
        Route::post('role-has-permission/{id}', [ResourceRoleHasPermissionsController::class,'rolehasperm']);
        Route::post('user-has-role/{id}', [ResourceRoleHasPermissionsController::class,'userhasrole']);
        Route::post('user-has-permission/{id}', [ResourceRoleHasPermissionsController::class,'userhasperm']);

        Route::post('rhp', [ResourceRoleHasPermissionsController::class,'rhp']);
        Route::post('uhp', [ResourceRoleHasPermissionsController::class,'uhp']);
        Route::post('uhr', [ResourceRoleHasPermissionsController::class,'uhr']);


        Route::get('permissions', [ResourceRoleHasPermissionsController::class,'index']);
        Route::post('permissions', [ResourcePermissionsController::class,'store']);
        Route::get('permission/{id}', [ResourcePermissionsController::class,'show']);
        Route::put('permission/{id}', [ResourcePermissionsController::class,'update']);
        Route::delete('permission/{id}', [ResourcePermissionsController::class,'destroy']);

    }
);

Route::get('user-permissions/{id}', [ResourceRoleHasPermissionsController::class,'userrolehasperm']);

